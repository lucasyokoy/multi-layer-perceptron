def weights_derivatives_numerical(original, storage_copy, h, layer_index, line_index, column_index):
	#make 1 copy of the original neural net (calculation copy)
	calculation_copy = deepcopy(original)
	#on calculation copy, make tiny change (h) to the value of one hyperparameter x
	calculation_copy.w[layer_index][line_index][column_index] += h
	#forward propagate calculation copy (from currente layer until end)
	calculation_copy.for_propagation_recursive(layer_index+1,original.a[0])
	#calculate cost_total for each, subtract and divide by h (dC/dx = lim h->0 (C(x)-C(x+h))/h)
	calculation_copy.cost_total_calculation()
	dC_dx = (original.cost_total-calculation_copy.cost_total)/h
	#store derivative on storage copy
	storage_copy.w[layer_index][line_index][column_index] =dC_dx
	#delete calculation copy and repeat until all hyperparameters have had its derivatives calculated
	del(calculation_copy)

def biases_derivatives_numerical(original, storage_copy, h, layer_index, element_index):
	#make 1 copy of the original neural net (calculation copy)
	calculation_copy = deepcopy(original)
	#on calculation copy, make tiny change (h) to the value of one hyperparameter x
	calculation_copy.b[layer_index][element_index] += h
	#forward propagate calculation copy (from currente layer until end)
	calculation_copy.for_propagation_recursive(layer_index+1,original.a[0])
	#calculate cost_total for each, subtract and divide by h (dC/dx = lim h->0 (C(x)-C(x+h))/h)
	calculation_copy.cost_total_calculation()
	dC_dx = (original.cost_total-calculation_copy.cost_total)/h
	#store derivative on storage copy
	storage_copy.b[layer_index][element_index] =dC_dx
	#delete calculation copy and repeat until all hyperparameters have had its derivatives calculated
	del(calculation_copy)

def gradient_numerical(original):
	"""neural net must have been forward propagated and have it's own cost_total calculated prior to calling this method"""
	#make 1 copy of the original neural net (storage copy)
	storage_copy = deepcopy(original)
	h=0.00000000001
	#parallel_process_list = []
	#gradient for weights
	for layer_index, layer in enumerate(original.w):
		for line_index, line in enumerate(layer):
			for column_index, cell in enumerate(line):
				weights_derivatives_numerical(original, storage_copy, h, layer_index, line_index, column_index)
				#calculate weights derivatives in parallel
				#arguments = (original, storage_copy, h, layer_index, line_index, column_index)
				#derivative_calculation = multiprocessing.Process(target=weights_derivatives_numerical, args=arguments)
				#derivative_calculation.start()
				#parallel_process_list.append(derivative_calculation)
	#gradient for biases
	for layer_index, layer in enumerate(original.b):
		for element_index, element in enumerate(layer):
			biases_derivatives_numerical(original, storage_copy, h, layer_index, element_index)
			#calculate bias derivatives in parallel
			#arguments = (original, storage_copy, h, layer_index, element_index)
			#derivative_calculation = multiprocessing.Process(target=biases_derivatives_numerical, args=arguments)
			#derivative_calculation.start()
			#parallel_process_list.append(derivative_calculation)

	#for process in parallel_process_list:
	#	process.join()
	#return storage copy
	return storage_copy
