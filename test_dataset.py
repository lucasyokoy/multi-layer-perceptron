import numpy as np

#the first iteration will see if the model can approximate the following function:
#3 inputs, 4 outputs
def test_function(input):
    """input must be an np.array with 3 elements"""
    return np.array([np.sin(input[0]),np.sin(input[1]),np.sin(input[2]),np.sin(np.sum(input))])

def generate_batch(function, batch_size, inputs_range, inputs_size, *batch_inputs):
    """ optional inputs list must be a list or np.arrays. Input range must be a tuple of 2 integers defining the range of the random numbers generated"""
    #generate list of batch_inputs (optional *batch_inputs if provided)
    if(batch_inputs):
        input_list = list(batch_inputs[0])
    else:
        #batch_range must be a tuple defining the range for the random numbers generated
        input_list = [np.random.uniform(low=inputs_range[0],high=inputs_range[1],size = inputs_size) for _ in range(batch_size)]
    #generate list of outputs (function(inputs))
    outputs = [function(input) for input in input_list]
    #join the 2 lists in a larger list np.meshgrid(inputs,outputs) and return it
    #returns a tuple where element[0] is the list of inputs, and element[1] is the list of outputs.
    #the secodn index[][n] defines the nth input-output pair
    return input_list,outputs
