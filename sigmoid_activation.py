from functools import lru_cache
from functools import reduce
from numpy import exp

def activation(x):
	return 1/(1+exp(-x))   #sigmoid squishification function (between 0 and 1) you can put whatever function

def dC_dw(original,layer_index, line_index ,element_index):
	#dC_dw[layer][line][column] = dC_da*da_dz*dz_dw
	return dC_da(original,layer_index,line_index)*da_dz(original,layer_index,line_index)*dz_dw(original,layer_index,element_index)
	
def dC_db(original,layer_index,line_index):
	#dC_dw = dC_da*da_dz*dz_db
	return dC_da(original,layer_index,line_index)*da_dz(original,layer_index,line_index)*dz_db(original)
	
def da_dz(original,layer_index,line_index):
	return (original.a[layer_index+1][line_index])*(1-original.a[layer_index+1][line_index])
	
def dz_da_l_minus_one(original, layer_index, line_index, element_index):
	"""dz[layer][element]_da[layer-1][element] = w[layer][line][element] """
	return original.w[layer_index][line_index][element_index]
	
def dz_db(original):
	return 1

def dz_dw(original,layer_index,element_index):
	return original.a[layer_index][element_index]
	
@lru_cache(maxsize = None)
def dC_da(original,layer_index,line_index):
	if (original.a[layer_index+1] is original.a[-1]):
		return 2*(original.target_matrix[line_index]-original.a[-1][line_index])/len(original.a[-1])
	else:
		#return sum of dC_da[layer+1] troughout next_layer
		#line_index is the index for the current element in layer L(equivalento to element_index)
		#line is the index for items in layer L+1(equivalento to line_index)
	#	next_layer_indexes = range(len(original.a[layer_index+1]))
	#	chain_rule_sum_list = [dC_da(original,layer_index+1,line)*da_dz(original,layer_index+1,line)*dz_da_l_minus_one(original,layer_index+1,line,line_index) for line in next_layer_indexes]
		element = line_index
		next_layer = layer_index+1
		next_layer_lines = range(len(original.a[next_layer+1]))
		chain_rule_sum_list = []
		for line in next_layer_lines:
		#line = 0
		#while line < len(original.a[next_layer+1]):
			derivative =dC_da(original,next_layer,line)*da_dz(original,next_layer,line)*dz_da_l_minus_one(original,next_layer,line,element)
			chain_rule_sum_list.append(derivative)
			#line += 1
		return reduce(lambda accumulator,x: accumulator+x, chain_rule_sum_list)

