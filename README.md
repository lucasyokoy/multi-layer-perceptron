# Multi Layer Perceptron

My first attempt at producing a functional AI from scratch (without AI frameworks).

note1: the docstring for the gradient calculation functions states that the neural net must have been previously forward propagated before calling the gradient calculator over it.
Otherwise, interestingly, the output displays chaotic behavior (tiny changes in any hyper parameters yield massive changes in the output).
For more information, research the butterfly effect and chaos theory.
