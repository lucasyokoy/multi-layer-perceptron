#!/usr/bin/env python3
from sigmoid_activation import activation
import numpy as np
from copy import deepcopy
from statistics import mean

class multi_layer_perceptron:
	def __init__(self, layers, n):
		self.structure = layers
		self.size = len(layers) #size is the number of layers in a Neural_network
		#layers is an array[n] where each element is the number of neurons in the n-th layer
		self.w = []#array[k][j][i], where each array is the weights between the layers k and k-1 (by default, k in this array equals the number_of_layers-1)
		self.a = []#array[k][j], where each element [j] is the value of the neuron j in layer k (j doesn't have to be the same for all layers, thus this is an array of arrays[j][1], not a 3D array)
		self.z = []#array for the linear combination in each layer, prior to activation function
		self.b = []#array[k][j], where each element [j] is the bias of the neuron j in layer k
		self.learning_rate = n
		if(self.size>=2):
			for number in range(self.size-1):
				#initializes all layers using numpy matrix initialization tools.
				#I used random, but you can use whatever you want, or even set them manually later.
				weights = np.random.uniform(low=-2,high=2,size=(layers[number+1],layers[number]))
				avalues = np.random.uniform(low=-2,high=2,size=(layers[number]))
				biases = np.random.uniform(low=-2,high=2,size=(layers[number+1]))
				zvalues = np.zeros(layers[number])
				self.w.append(weights)
				self.a.append(avalues)
				self.z.append(zvalues)
				self.b.append(biases)
				#Example of layers = [5, 6, 4, 3]
				#a[0] = [i1, i2, i3, i4 ,i5] inputs
				#w[0] = [[w11, w12, w13, w14, w15][w21, w22, w23, w24, w25]...[w51, w52, w53, w54, w55]] 6x5 weights between layers 0 and 1
				#b[0] = [0, 0, 0, 0, 0] biases of 0th layer (none)
				#a[1] = [a1, a2, a3, a4 ,a5, a6] values on layer 1
				#w[1] = [[w11, w12, w13, w14, w15][w21, w22, w23, w24, w25]...[w51, w52, w53, w54, w55]] 6x5 weights between layers 1 and 2
				#b[1] = [b1, b2, b3, b4, b5, b6] biases of 1th layer
			#the last layer must have no weights
			avalues = np.random.uniform(low=-2,high=2,size=(layers[-1]))
			zvalues = np.zeros(layers[-1])
			self.a.append(avalues)
			self.z.append(zvalues)
			#layers must have at least 2 elements, [0] for the input layer and [last] for the output layer
		else: print("Less than 2 layers. Could not initialize.\n")

	def __str__(self):
		#displays the values in each neuron in each layer within the network
		string1= "structure created "+str(self.structure)
		print("a:")
		print(self.a)
		for layer in self.a:
			print(layer.shape)
		print("w:")
		print(self.w)
		for layer in self.w:
			print(layer.shape)
		print("b:")
		print(self.b)
		for layer in self.b:
			print(layer.shape)
		return string1

	def __add__(self,x):
		"""NOT COMMUTATIVE if x is a constant, it must come after self (self+x) otherwise an error will be raised"""
		sum_accumulator = deepcopy(self)
		if(type(x) is int or type(x) is float):
			sum_accumulator.w = [weights_self+x for weights_self in self.w]
			sum_accumulator.b = [biases_self+x for biases_self in self.b]
		else:
			sum_accumulator.w = [weights_self+weights_x for weights_self,weights_x in zip(self.w,x.w)]
			sum_accumulator.b = [biases_self+biases_x for biases_self,biases_x in zip(self.b,x.b)]
		return sum_accumulator

	def __mul__(self,x):
		"""NOT COMMUTATIVE if x is a constant, it must come after self (self*x) otherwise an error will be raised"""
		product = deepcopy(self)
		#if type of second operand (x) is not MLP, multiply each element in array by constant x
		if(type(x) is int or type(x) is float):
			product.w = [weights_self*x for weights_self in self.w]
			product.b = [biases_self*x for biases_self in self.b]
		else:
			product.w = [weights_self*weights_x for weights_self,weights_x in zip(self.w,x.w)]
			product.b = [biases_self*biases_x for biases_self,biases_x in zip(self.b,x.b)]
		return product

	def __pow__(self,x):
		"""NOT COMMUTATIVE if x is a constant, it must come after self (self**x) otherwise an error will be raised"""
		power = deepcopy(self)
		#if type of second operand (x) is not MLP, multiply each element in array by constant x
		if(type(x) is int or type(x) is float):
			power.w = [weights_self**x for weights_self in self.w]
			power.b = [biases_self**x for biases_self in self.b]
		else:
			power.w = [weights_self**weights_x for weights_self,weights_x in zip(self.w,x.w)]
			power.b = [biases_self**biases_x for biases_self,biases_x in zip(self.b,x.b)]
		return power

	def __sub__(self,x):
		return self + x*(-1)

	def __truediv__(self,x):
		return self*(x**-1)

	def set_input(self, provided_input):
		if(provided_input.shape != self.a[0].shape):
			print("The size of input layer provided is incorrect.")
		else:
			del self.a[0]
			self.a.insert(0, provided_input) #replaces first layer (input layer)

	def get_output(self):
		return self.a[-1] #returns last layer (output layer)

	def for_propagation_iterative(self, input_layer, initial_layer):
		#computes the output, given the input layer(from 1 to n) a[k] = sigmoid(a[k-1]*w[k-1]+b[k]) k from 1 to size
		self.set_input(input_layer)
		for layer in range(initial_layer,self.size):
			self.z[layer] = self.w[layer-1] @ self.a[layer-1]+ self.b[layer-1]
			self.a[layer] = activation_function(self.z[layer])

	def for_propagation_recursive(self, layer, *args):
		#computes the output, given the input layer a[k] = sigmoid(a[k-1]*w[k-1]+b[k]) k from 1 to size
		if(layer == 1):
			try:
				self.set_input(args[0])
			except:
				print("Input layer must be provided.")
				return
		self.z[layer] = self.w[layer-1] @ self.a[layer-1]+ self.b[layer-1]
		self.a[layer] = activation_function(self.z[layer])
		if(self.a[layer] is self.a[-1]): #if it's the last layer, just return output and end. otherwise, recursively propagate into next layer
			return
		else:
			self.for_propagation_recursive(layer+1)

	def activation_function(x):
		return activation(x)
