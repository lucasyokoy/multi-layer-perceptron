gradient_numerical(original)import multi_layer_perceptron as MLP
import numpy as np
from statistics import mean
from functools import reduce
import multiprocessing
from sigmoid_activation import dC_dw
from sigmoid_activation import dC_db
from gradient_numerical import gradient_numerical
#import concurrent.futures
from copy import deepcopy

class supervised_learning(MLP.multi_layer_perceptron):
	def __init__(self, layers, n):
		MLP.multi_layer_perceptron.__init__(self, layers,n)
		self.target_matrix= np.array([])
		self.cost_matrix= np.array([])
		self.cost_total = 0

	def set_target_matrix(self, provided_target):
		try:
			if(self.get_output().shape != provided_target.shape):
				raise LookUpError
			self.target_matrix = provided_target
		except LookupError:
			print("Sizes of target matrix and output matrix don't match.")

	def get_cost_total(self):
		return self.cost_total

	def cost_matrix_calculation(self):
		#cost_matrix = (target-output)**2
		cost_matrix_temporary= self.target_matrix-self.get_output()
		self.cost_matrix = np.array(list(map(lambda x: x**2, cost_matrix_temporary)))

	def cost_total_calculation(self):
		#mean square error
		self.cost_matrix_calculation()
		self.cost_total = mean(self.cost_matrix)

	def back_propagation_algebraic(self, batch_target_matrixes, batch_inputs):
		"""batch_target_matrixes should receive a list with target matrixes for the epoch"""
		#Verify wether each input has it's respective target
		if (len(batch_target_matrixes) !=len(batch_inputs)): raise ValueError("Sizes of matrixes in input and target don't match")
		#calculate gradient for each element in batch in parallel
		batch_size = len(batch_inputs)
		average_gradient = deepcopy(self)
		average_gradient *= 0
		args = list(zip(batch_target_matrixes,batch_inputs))
		pool = multiprocessing.Pool(processes = batch_size)
		gradient_list = pool.map(self.epoch_algebraic_parallel,args)
		#take average (avg = sum/total_ammount)
		average_gradient = reduce(lambda acc,y:acc+y, gradient_list)
		average_gradient /= batch_size
		#update: self = self-self.n*average_gradient
		return self.gradient_descent(average_gradient)

	def epoch_algebraic_parallel(self, args):
		self.set_target_matrix(args[0])
		self.for_propagation_recursive(1, args[1])
		self.cost_total_calculation()
		gradient_algebraic = self.gradient_algebraic()
		return gradient_algebraic

	def back_propagation_numerical(self, batch_target_matrixes, batch_inputs):
		"""batch_target_matrixes should receive a list with target matrixes for the epoch"""
		#Verify wether each input has it's respective target
		if (len(batch_target_matrixes) !=len(batch_inputs)): raise ValueError
		#calculate gradient for each element in batch in parallel
		batch_size = len(batch_inputs)
		average_gradient = deepcopy(self)
		average_gradient *= 0
		args = list(zip(batch_target_matrixes,batch_inputs))
		pool = multiprocessing.Pool(processes = batch_size)
		gradient_list = pool.map(self.epoch_numerical_parallel,args)
		#take average (avg = sum/total_ammount)
		average_gradient = reduce(lambda acc,y:acc+y, gradient_list)
		average_gradient /= batch_size
		#update: self = self-self.n*average_gradient
		return (self.gradient_descent(average_gradient),average_gradient)

	def epoch_numerical_parallel(self, args):
		self.set_target_matrix(args[0])
		self.for_propagation_recursive(1, args[1])
		self.cost_total_calculation()
		gradient_numerical = gradient_numerical(self)
		return gradient_numerical

	def gradient_descent(self,gradient):
		"""self_t+1 = self_t - gradient(cost)*learning_rate """
		return self - gradient*self.learning_rate

	def gradient_algebraic(self):
		"""neural net must have been forward propagated and have it's own cost_total calculated prior to calling this method"""
		#make 1 copy of the original neural net (storage copy)
		storage_copy = deepcopy(self)
		#define derivatives of each hyperparameters, relative to it's imediate variables (algebraically).
		#start from last layer
		#gradient of weights
		#layer_index(L) = weights between L and L+1 activation layers
		#line_index = activations on layer L+1 and it's respective weights
		#element_index = activations on layer L and it's respective weights
		for layer_index in range( len(self.w) - 1, -1, -1):
			for line_index in range(len(self.w[layer_index])):
				for element_index in range(len(self.w[layer_index][line_index])):
					storage_copy.w[layer_index][line_index][element_index]=dC_dw(self,layer_index, line_index, element_index)
		#gradient of biases
		for layer_index in range( len(self.b) - 1, -1, -1):
			for line_index in range(len(self.b[layer_index])):
		#the second loop should be equivalent to the first
				storage_copy.b[layer_index][line_index]=dC_db(self,layer_index, line_index)
		#return the gradient stored on the copy
		return storage_copy

#relative differences are minimal for any given layer (rarely over 10%, often under 1%)
